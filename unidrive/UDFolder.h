//
//  UDFolder.h
//  
//
//  Created by Rustam Nadrshin on 5/21/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UDFile, UDFolder;

@interface UDFolder : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * path;
@property (nonatomic, retain) NSNumber * forDeleteDropbox;
@property (nonatomic, retain) NSNumber * forDeleteOneDrive;
@property (nonatomic, retain) NSNumber * forDeleteGoogleDrive;
@property (nonatomic, retain) NSSet *files;
@property (nonatomic, retain) NSSet *folders;
@property (nonatomic, retain) UDFolder *parent;
@end

@interface UDFolder (CoreDataGeneratedAccessors)

- (void)addFilesObject:(UDFile *)value;
- (void)removeFilesObject:(UDFile *)value;
- (void)addFiles:(NSSet *)values;
- (void)removeFiles:(NSSet *)values;

- (void)addFoldersObject:(UDFolder *)value;
- (void)removeFoldersObject:(UDFolder *)value;
- (void)addFolders:(NSSet *)values;
- (void)removeFolders:(NSSet *)values;

@end
