//
//  UDFile.h
//  
//
//  Created by Rustam Nadrshin on 5/19/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UDFolder;

@interface UDFile : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * localPath;
@property (nonatomic, retain) NSNumber * ready;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * drivePath;
@property (nonatomic, retain) NSDate * updatedDate;
@property (nonatomic, retain) NSNumber * forDelete;
@property (nonatomic, retain) UDFolder *parent;

@end
