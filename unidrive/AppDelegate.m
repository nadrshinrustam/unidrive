//
//  AppDelegate.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/14/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "AppDelegate.h"
#import <DropboxSDK/DropboxSDK.h>
#import "UDDropboxManager.h"
#import "UDOneDriveManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [UDCoreDataManager sharedInstance];
    [UDDropboxManager sharedInstance];
    [UDOneDriveManager sharedInstance];
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url sourceApplication:(NSString *)source annotation:(id)annotation
{
    if ([[DBSession sharedSession] handleOpenURL:url])
    {
        if ([[DBSession sharedSession] isLinked])
        {
            NSLog(@"App linked successfully!");
        }
        return YES;
    }
    return NO;
}

@end
