//
//  UDDetailViewController.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/19/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UDDetailViewController : UIViewController

@property (nonatomic, strong) NSNumber * fileId;

@end
