//
//  UDDetailViewController.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/19/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "UDDetailViewController.h"

@interface UDDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *img;

@end

@implementation UDDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextUpdated) name:NOTIFICATION_CONTEXT_UPDATED object:nil];
    [self reloadData];
}

- (void)reloadData
{
    UDFile * file = [[UDCoreDataManager sharedInstance] getFileWithId:_fileId];
    if (file == nil)
    {
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
    else
    {
        BOOL exist = [[NSFileManager defaultManager] fileExistsAtPath:[file localPath]];
        if (exist)
        {
            UIImage * image = [UIImage imageWithContentsOfFile:[file localPath]];
            [_img setImage:image];
        }
        [[self navigationItem] setTitle:[file name]];
    }
}

- (void)contextUpdated
{
    [self reloadData];
}

@end
