//
//  UDDropboxManager.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/17/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#define DROPBOX_APP_KEY         @"6vhh6z4rzipijvt"
#define DROPBOX_APP_SECRET      @"15b7x29759wdl3y"

#define DROPBOX_ROOT    @"/"

#import "UDDropboxManager.h"
#import <DropboxSDK/DropboxSDK.h>


@interface UDDropboxManager () <DBSessionDelegate, DBNetworkRequestDelegate, DBRestClientDelegate>

@property (nonatomic, strong) DBRestClient * client;
@property (nonatomic) BOOL cycleUpdate;
@property (nonatomic) BOOL updating;
@property (nonatomic) NSInteger connectionCount;
@property (nonatomic, strong) NSMutableArray * downloadList;
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation UDDropboxManager

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static UDDropboxManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        DBSession * session = [[DBSession alloc] initWithAppKey:DROPBOX_APP_KEY appSecret:DROPBOX_APP_SECRET root:kDBRootDropbox];
        [session setDelegate:self];
        [DBSession setSharedSession:session];
        [DBRequest setNetworkRequestDelegate:self];
        
        _client = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        [_client setDelegate:self];
        
        _downloadList = [[NSMutableArray alloc] init];
        
        NSString * localPath = [NSString stringWithFormat:@"%@/Dropbox",[[UDCoreDataManager sharedInstance] applicationDocumentsDirectory]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:localPath])
        {
            NSError * error = nil;
            if ([[NSFileManager defaultManager] createDirectoryAtPath:localPath withIntermediateDirectories:NO attributes:nil error:&error])
                NSLog(@"Folder created %@",localPath);
            else
                NSLog(@"Could not create a folder %@ error - %@",localPath, error);
        }
        else
        {
            NSLog(@"Folder exist %@",localPath);
        }
    }
    return self;
}

- (void)setCycleUpdate:(BOOL)cycleUpdate
{
    _cycleUpdate = cycleUpdate;
}

- (BOOL)isLinked
{
    return [[DBSession sharedSession] isLinked];
}

- (void)update
{
    if (!_updating)
    {
        _updating = YES;
        _connectionCount = 1;
        [[UDCoreDataManager sharedInstance] setAllFilesToDelete:DROPBOX_FILE];
        [[UDCoreDataManager sharedInstance] setAllFoldersToDelete:DROPBOX_FILE];
        [_client loadMetadata:DROPBOX_ROOT];
    }
}

- (void)loadFiles
{
    UDFile * file = [_downloadList firstObject];
    if (file != nil)
    {
        NSString * parentPath = [[file parent] path];
        NSString * localPath = [NSString stringWithFormat:@"%@/Dropbox%@",[[UDCoreDataManager sharedInstance] applicationDocumentsDirectory], parentPath];
        NSString * filePath = [NSString stringWithFormat:@"%@%@", localPath, [file name]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:localPath])
        {
            NSError * error = nil;
            if ([[NSFileManager defaultManager] createDirectoryAtPath:localPath withIntermediateDirectories:NO attributes:nil error:&error])
            {
                UDLog(@"Folder created %@",localPath);
            }
            else
            {
                UDLog(@"Could create a folder %@ %@",localPath, error);
            }
        }
        else
        {
            UDLog(@"Folder exist %@",localPath);
        }
        [_client loadFile:[file drivePath] intoPath:filePath];
    }
    else
    {
        _updating = NO;
        UDLog(@"DROPBOX updated");
        if (_cycleUpdate)
        {
            _timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                      target:self
                                                    selector:@selector(timerTick)
                                                    userInfo:nil
                                                     repeats:NO];
        }
    }
}

- (void)timerTick
{
    if (_cycleUpdate)
    {
        [self update];
    }
}

- (void)loginFromCotroller:(UIViewController*)rootController
{
    if (![[UDDropboxManager sharedInstance] isLinked])
    {
        [[DBSession sharedSession] linkFromController:rootController];
    }
}

#pragma mark -
#pragma mark - Dropbox Rest Client Delegate

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata
{
    UDFolder * parentFolder = nil;
    if ([[metadata path] isEqualToString:DROPBOX_ROOT])
    {
        parentFolder = [[UDCoreDataManager sharedInstance] getFolderWithPath:[metadata path]];
    }
    else
    {
        parentFolder = [[UDCoreDataManager sharedInstance] getFolderWithPath:[NSString stringWithFormat:@"%@/",[metadata path]]];
    }
    if (metadata.isDirectory)
    {
        NSLog(@"Folder '%@' contains:", metadata.path);
        for (DBMetadata * data in metadata.contents)
        {
            if (data.isDirectory)
            {
                NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"name == %@", data.filename];
                UDFolder * folder = [[[[parentFolder folders] filteredSetUsingPredicate:entityPredicate] allObjects] firstObject];
                if (folder != nil)
                {
                    [folder setForDeleteDropbox:[NSNumber numberWithBool:NO]];
                    NSLog(@"Folder  %@  already exist", data.filename);
                }
                else
                {
                    folder = [[UDCoreDataManager sharedInstance] newFolderWithName:data.filename];
                    [folder setPath:[NSString stringWithFormat:@"%@/",[data path]]];
                    [parentFolder addFoldersObject:folder];
                    NSLog(@"Folder  %@  created", [data filename]);
                }
                [_client loadMetadata:[data path]];
                _connectionCount++;
            }
            else
            {
                NSCompoundPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[NSPredicate predicateWithFormat:@"name == %@", data.filename],
                                                                                                       [NSPredicate predicateWithFormat:@"type == %d", DROPBOX_FILE]]];
                UDFile * file = [[[[parentFolder files] filteredSetUsingPredicate:predicate] allObjects] firstObject];
                if (file != nil)
                {
                    [file setForDelete:[NSNumber numberWithBool:NO]];
                    if ([[file updatedDate] isEqualToDate:data.lastModifiedDate])
                    {
                        NSLog(@"File    %@  didn't change", data.filename);
                    }
                    else
                    {
                        NSLog(@"File    %@  changed", data.filename);
                        [file setReady:[NSNumber numberWithBool:NO]];
                        [_downloadList addObject:file];
                        NSError *error;
                        if ([[NSFileManager defaultManager] removeItemAtPath:[file localPath] error:&error])
                        {
                            NSLog(@"File    %@  deleted from local path %@", file.name, file.localPath);
                        }
                        else
                        {
                            NSLog(@"Could not delete file from path %@ -:%@ ",[file localPath], [error localizedDescription]);
                        }
                    }
                }
                else
                {
                    UDFile * file = [[UDCoreDataManager sharedInstance] newFileWithName:data.filename];
                    [file setType:[NSNumber numberWithInt:DROPBOX_FILE]];
                    [file setDrivePath:[data path]];
                    [parentFolder addFilesObject:file];
                    [_downloadList addObject:file];
                    NSLog(@"File    %@  created", data.filename);
                }
            }
        }
    }
    _connectionCount--;
    if (_connectionCount == 0)
    {
        [[UDCoreDataManager sharedInstance] deleteAllSettedFiles:DROPBOX_FILE];
        [[UDCoreDataManager sharedInstance] deleteAllSettedFolders];
        [[UDCoreDataManager sharedInstance] saveContext];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
        [self loadFiles];
    }
}

- (void)restClient:(DBRestClient *)client loadMetadataFailedWithError:(NSError *)error
{
    NSLog(@"Error loading metadata: %@", error);
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)localPath contentType:(NSString *)contentType metadata:(DBMetadata *)metadata
{
    NSLog(@"File loaded into path: %@", localPath);

    UDFile * file = [_downloadList firstObject];
    [file setUpdatedDate:metadata.lastModifiedDate];
    [file setLocalPath:localPath];
    [file setReady:[NSNumber numberWithBool:YES]];
    [[UDCoreDataManager sharedInstance] saveContext];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
    [_downloadList removeObject:file];
    [self loadFiles];
}

- (void)restClient:(DBRestClient *)client loadFileFailedWithError:(NSError *)error
{
    UDFile * file = [_downloadList firstObject];
    [_downloadList removeObject:file];
    [self loadFiles];
    NSLog(@"There was an error loading the file: %@", error);
}


#pragma mark -
#pragma mark Dropbox Network Request Delegate

- (void)networkRequestStarted
{
    
}

- (void)networkRequestStopped
{
    
}


#pragma mark -
#pragma mark - Dropbox Session Delegate

- (void)sessionDidReceiveAuthorizationFailure:(DBSession *)session userId:(NSString *)userId
{
    NSLog(@"sessionDidReceiveAuthorizationFailure %@", userId);
}

@end
