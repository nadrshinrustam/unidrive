//
//  UDFolderViewController.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/16/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UDFolderViewController : UIViewController

@property (nonatomic, strong) NSNumber * folderId;

@end
