//
//  UDCoreDataManager.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/15/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface UDCoreDataManager : NSObject

+ (id)sharedInstance;

- (NSManagedObjectContext *)masterManagedObjectContext;
- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (void)saveContext;
- (NSString *)applicationDocumentsDirectory;

- (NSNumber*)getNextIdForFolder;

- (NSNumber*)getNextIdForFile;

- (UDFolder*)newFolderWithName:(NSString*)name;

- (UDFile*)newFileWithName:(NSString*)name;

- (UDFolder*)getRootFolder;

- (UDFolder*)getFolderWithId:(NSNumber*)folderId;

- (UDFolder*)getFolderWithPath:(NSString*)path;

- (UDFile*)getFileWithId:(NSNumber*)fileId;

- (void)setAllFilesToDelete:(NSInteger)type;

- (void)deleteAllSettedFiles:(NSInteger)type;

- (void)setAllFoldersToDelete:(NSInteger)type;

- (void)deleteAllSettedFolders;

@end
