//
//  Globals.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/16/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#ifndef unidrive_Globals_h
#define unidrive_Globals_h

enum UDFileType{
    GOOGLE_FILE = 1,
    ONE_DRIVE_FILE,
    DROPBOX_FILE
};

//#define GOOGLE_FILE     1
//#define DROPBOX_FILE    2
//#define ONE_DRIVE_FILE  3

#define CELL_IDENTIFIER     @"cell"
#define SEGUE_IDENTIFIER    @"segue"
#define SEGUE_DETAIL        @"detail"

#define EMPTY_STRING        @""

#define NOTIFICATION_CONTEXT_UPDATED    @"contextUpdated"

#endif
