//
//  UDOneDriveManager.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/18/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UDOneDriveManager : NSObject

+ (id)sharedInstance;

- (void)update;

- (void)loginFromCotroller:(UIViewController*)rootController;

- (void)setCycleUpdate:(BOOL)cycleUpdate;

@end
