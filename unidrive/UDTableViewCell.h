//
//  UDTableViewCell.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/16/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UDTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *source;

- (void)initWithName:(NSString*)name;

- (void)initWithName:(NSString*)name andSource:(int)source;

@end
