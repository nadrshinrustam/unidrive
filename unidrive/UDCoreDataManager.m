//
//  UDCoreDataManager.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/15/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "UDCoreDataManager.h"

@interface UDCoreDataManager ()

@property (strong, nonatomic) NSManagedObjectContext *masterManagedObjectContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation UDCoreDataManager

@synthesize masterManagedObjectContext = _masterManagedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static UDCoreDataManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        UDFolder * root = [self getRootFolder];
        if (root == nil)
        {
            UDFolder * root = [self newFolderWithName:@"/"];
            [root setPath:@"/"];
        }
    }
    return self;
}

- (NSManagedObjectContext *)masterManagedObjectContext
{
    if (_masterManagedObjectContext != nil)
        return _masterManagedObjectContext;

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        _masterManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_masterManagedObjectContext performBlockAndWait:^{
            [_masterManagedObjectContext setPersistentStoreCoordinator:coordinator];
        }];
    }
    return _masterManagedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
        return _managedObjectModel;
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
        return _persistentStoreCoordinator;
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    NSString *newDBDirectory = [[self applicationDocumentsDirectory] stringByAppendingPathComponent:@"unidrive.sqlite"];
    NSURL *storeUrl = [NSURL fileURLWithPath:newDBDirectory];
    NSDictionary *pscOptions = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO], NSInferMappingModelAutomaticallyOption, nil];

    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:pscOptions error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _persistentStoreCoordinator;
}

- (void)saveContext
{
    NSError *error = nil;
    if (self.masterManagedObjectContext != nil)
    {
        [self.masterManagedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];

        if ([self.masterManagedObjectContext hasChanges] && ![self.masterManagedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Application's Documents directory

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

#pragma mark - Other Utils

- (NSNumber*)getNextIdForFolder
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UDFolder class])];
    fetchRequest.fetchLimit = 1;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO]];
    UDFolder * lastFolder = [self.masterManagedObjectContext executeFetchRequest:fetchRequest error:nil].lastObject;
    if (lastFolder == nil)
        return [NSNumber numberWithInt:1];
    else
        return [NSNumber numberWithInt:[[lastFolder id] intValue] + 1];
}

- (NSNumber*)getNextIdForFile
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UDFile class])];
    fetchRequest.fetchLimit = 1;
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"id" ascending:NO]];
    UDFolder * lastFile = [self.masterManagedObjectContext executeFetchRequest:fetchRequest error:nil].lastObject;
    if (lastFile == nil)
        return [NSNumber numberWithInt:1];
    else
        return [NSNumber numberWithInt:[[lastFile id] intValue] + 1];
}

- (UDFolder*)newFolderWithName:(NSString*)name
{
    UDFolder * folder = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UDFolder class])
                                                      inManagedObjectContext:[self masterManagedObjectContext]];
    [folder setId:[self getNextIdForFolder]];
    [folder setName:name];
    [self saveContext];
    return folder;
}

- (UDFile*)newFileWithName:(NSString*)name
{
    UDFile * file = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UDFile class])
                                                  inManagedObjectContext:[self masterManagedObjectContext]];
    [file setId:[self getNextIdForFile]];
    [file setName:name];
    [self saveContext];
    return file;
}

- (UDFolder*)getRootFolder
{
    return [self getFolderWithId:[NSNumber numberWithInt:1]];
}

- (UDFolder*)getFolderWithId:(NSNumber*)folderId
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFolder class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"id == %d",[folderId integerValue]];
    [request setPredicate:entityPredicate];
    
    return [[self.masterManagedObjectContext executeFetchRequest:request error:nil] firstObject];
}

- (UDFolder*)getFolderWithPath:(NSString*)path
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFolder class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"path == %@", path];
    
    [request setPredicate:entityPredicate];
    
    return [[self.masterManagedObjectContext executeFetchRequest:request error:nil] firstObject];
}


- (UDFile*)getFileWithId:(NSNumber*)fileId
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFile class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"id == %d",[fileId integerValue]];
    [request setPredicate:entityPredicate];
    
    return [[self.masterManagedObjectContext executeFetchRequest:request error:nil] firstObject];
}

- (void)setAllFilesToDelete:(NSInteger)type
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFile class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"type == %d",type];
    [request setPredicate:entityPredicate];
    
    NSArray * files = [self.masterManagedObjectContext executeFetchRequest:request error:nil];
    for (UDFile * file in files)
    {
        [file setForDelete:[NSNumber numberWithBool:YES]];
    }
    [self saveContext];
}

- (void)deleteAllSettedFiles:(NSInteger)type
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFile class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSCompoundPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[NSPredicate predicateWithFormat:@"forDelete == YES"],
                                                                                           [NSPredicate predicateWithFormat:@"type == %d", type]]];

    [request setPredicate:predicate];
    
    NSArray * files = [self.masterManagedObjectContext executeFetchRequest:request error:nil];
    for (UDFile * file in files)
    {
        NSError * error = nil;
        if ([[NSFileManager defaultManager] removeItemAtPath:[file localPath] error:&error])
        {
            UDLog(@"File %@ deleted", [file localPath]);
        }
        else
        {
            UDLog(@"Could not delete file %@ error = %@", [file localPath], error);
        }
        [[self masterManagedObjectContext] deleteObject:file];
    }
    [self saveContext];
}

- (void)setAllFoldersToDelete:(NSInteger)type
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFolder class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];

    NSArray * folders = [self.masterManagedObjectContext executeFetchRequest:request error:nil];
    for (UDFolder * folder in folders)
    {
        if (![[folder path] isEqualToString:[[self getRootFolder] path]])
        {
            if (type == DROPBOX_FILE)
            {
                [folder setForDeleteDropbox:[NSNumber numberWithBool:YES]];
            }
            else if (type == ONE_DRIVE_FILE)
            {
                [folder setForDeleteOneDrive:[NSNumber numberWithBool:YES]];
            }
            else if (type == GOOGLE_FILE)
            {
                [folder setForDeleteGoogleDrive:[NSNumber numberWithBool:YES]];
            }
        }
    }
    [self saveContext];
}

- (void)deleteAllSettedFolders
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass([UDFolder class])
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    
    NSCompoundPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[NSPredicate predicateWithFormat:@"forDeleteDropbox == YES"],
                                                                                           [NSPredicate predicateWithFormat:@"forDeleteOneDrive == YES"],
                                                                                           [NSPredicate predicateWithFormat:@"forDeleteGoogleDrive == YES"]]];
    [request setPredicate:predicate];
    
    NSArray * folders = [self.masterManagedObjectContext executeFetchRequest:request error:nil];
    for (UDFolder * folder in folders)
    {
        [[self masterManagedObjectContext] deleteObject:folder];
    }
    [self saveContext];
}

- (void)deleteEntity:(__unsafe_unretained Class)entity withId:(NSNumber*)entityId
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:NSStringFromClass(entity)
                                                         inManagedObjectContext:[self masterManagedObjectContext]];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDescription];
    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"id == %d",[entityId intValue]];
    [request setPredicate:entityPredicate];
    NSManagedObject * object = [[self.masterManagedObjectContext executeFetchRequest:request error:nil] firstObject];
    [[self masterManagedObjectContext] deleteObject:object];
    [[self masterManagedObjectContext] save:nil];
}

@end
