//
//  UDFolderViewController.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/16/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "UDFolderViewController.h"
#import "UDDetailViewController.h"
#import "UDTableViewCell.h"

@interface UDFolderViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UDFolder * folder;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray * folders;
@property (nonatomic, strong) NSArray * files;

@end

@implementation UDFolderViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(contextUpdated) name:NOTIFICATION_CONTEXT_UPDATED object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)reloadData
{
    NSSortDescriptor * sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    if (_folderId != nil)
    {
        _folder = [[UDCoreDataManager sharedInstance] getFolderWithId:_folderId];
        if (_folder == nil)
        {
            [[self navigationController] popToRootViewControllerAnimated:YES];
        }
        else
        {
            [[self navigationItem] setTitle:[_folder name]];
            _folders = [[_folder folders] sortedArrayUsingDescriptors:@[sortDescriptor]];
            _files = [[_folder files] sortedArrayUsingDescriptors:@[sortDescriptor]];
            [[self tableView] reloadData];
        }
    }
    else
    {
        _folder = [[UDCoreDataManager sharedInstance] getRootFolder];
        [[self navigationItem] setTitle:@"Root folder"];
        _folders = [[_folder folders] sortedArrayUsingDescriptors:@[sortDescriptor]];
        _files = [[_folder files] sortedArrayUsingDescriptors:@[sortDescriptor]];
        [[self tableView] reloadData];
    }
}

- (void)contextUpdated
{
    [self reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:SEGUE_IDENTIFIER])
    {
        UDFolderViewController * destination = [segue destinationViewController];
        NSIndexPath * indexPath = sender;
        [destination setFolderId:[[_folders objectAtIndex:indexPath.row] id]];
    }
    else if ([[segue identifier] isEqualToString:SEGUE_DETAIL])
    {
        UDDetailViewController * destination = [segue destinationViewController];
        NSIndexPath * indexPath = sender;
        [destination setFileId:[[_files objectAtIndex:indexPath.row] id]];
    }
}

#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return [_folders count];
    }
    else
    {
        return [_files count];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0 && [_folders count] > 0)
    {
        return @"Folders";
    }
    else if (section == 1 && [_files count] > 0)
    {
        return @"Files";
    }
    else
    {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    if (indexPath.section == 0)
    {
        UDFolder * folder = [_folders objectAtIndex:indexPath.row];
        [cell initWithName:[folder name]];
    }
    else
    {
        UDFile * file = [_files objectAtIndex:indexPath.row];
        [cell initWithName:[file name] andSource:[[file type] intValue]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        [self performSegueWithIdentifier:SEGUE_IDENTIFIER sender:indexPath];
    }
    else
    {
        [self performSegueWithIdentifier:SEGUE_DETAIL sender:indexPath];
    }
}

@end
