//
//  UDSettingsViewController.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/14/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "UDSettingsViewController.h"
#import "UDDropboxManager.h"
#import "UDOneDriveManager.h"
#import "UDGoogleDriveManager.h"

@interface UDSettingsViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic) BOOL isDropboxUpdate;
@property (nonatomic) BOOL isOneDriveUpdate;
@property (nonatomic) BOOL isGoogleDriveUpdate;

@end

@implementation UDSettingsViewController


#pragma mark -
#pragma mark - Login

- (IBAction)loginDropbox
{
    [[UDDropboxManager sharedInstance] loginFromCotroller:self];
}

- (IBAction)loginOneDrive
{
    [[UDOneDriveManager sharedInstance] loginFromCotroller:self];
}

- (IBAction)loginGoogleDrive
{
    [self.navigationController pushViewController:[[UDGoogleDriveManager sharedInstance] loginCotroller] animated:YES];
}


#pragma mark -
#pragma mark - Update

- (IBAction)manageDropboxUpdate:(UIButton *)sender
{
    if (_isDropboxUpdate)
    {
        [sender setTitle:@"Start Dropbox Updating" forState:UIControlStateNormal];
        [[UDDropboxManager sharedInstance] setCycleUpdate:NO];
    }
    else
    {
        [sender setTitle:@"Stop Dropbox Updating" forState:UIControlStateNormal];
        [[UDDropboxManager sharedInstance] setCycleUpdate:YES];
        [self updateDropbox];
    }
    _isDropboxUpdate = !_isDropboxUpdate;
}

- (IBAction)manageOneDriceUpdate:(UIButton *)sender
{
    if (_isOneDriveUpdate)
    {
        [sender setTitle:@"Start One Drive Updating" forState:UIControlStateNormal];
        [[UDOneDriveManager sharedInstance] setCycleUpdate:NO];
    }
    else
    {
        [sender setTitle:@"Stop One Drive Updating" forState:UIControlStateNormal];
        [[UDOneDriveManager sharedInstance] setCycleUpdate:YES];
        [self updateOneDrive];
    }
    _isOneDriveUpdate = !_isOneDriveUpdate;
}

- (IBAction)manageGoogleDriveUpdate:(UIButton *)sender
{
    if (_isGoogleDriveUpdate)
    {
        [sender setTitle:@"Start Google Drive Updating" forState:UIControlStateNormal];
        [[UDGoogleDriveManager sharedInstance] setCycleUpdate:NO];
    }
    else
    {
        [sender setTitle:@"Stop Google Drive Updating" forState:UIControlStateNormal];
        [[UDGoogleDriveManager sharedInstance] setCycleUpdate:YES];
        [self updateGoogleDrive];
    }
    _isGoogleDriveUpdate = !_isGoogleDriveUpdate;
}

- (void)updateDropbox
{
    [[UDDropboxManager sharedInstance] update];
}

- (void)updateOneDrive
{
    [[UDOneDriveManager sharedInstance] update];
}

- (void)updateGoogleDrive
{
    [[UDGoogleDriveManager sharedInstance] update];
}


@end
