//
//  UDGoogleDriveManager.h
//  unidrive
//
//  Created by Rustam Nadrshin on 5/17/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UDGoogleDriveManager : NSObject

+ (id)sharedInstance;

- (void)update;

- (UIViewController*)loginCotroller;

- (void)setCycleUpdate:(BOOL)cycleUpdate;

@end
