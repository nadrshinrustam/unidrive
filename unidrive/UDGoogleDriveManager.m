//
//  UDGoogleDriveManager.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/17/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#define GOOGLE_APP_KEY          @"441549426583-pb9hi9r305qkcqoi0h8jijqeqn022ce8.apps.googleusercontent.com"
#define GOOGLE_APP_SECRET       @"8HZqPoUADr9PlPqi0L3lQT3F"

#define GOOGLE_ROOT_FOLDER      @"root"

static NSString *const kKeychainItemName = @"Google Drive Quickstart";

#import <Google-API-Client/GTLDrive.h>
#import <Google-API-Client/GTLDriveFileList.h>
#import <gtm-oauth2/GTMOAuth2ViewControllerTouch.h>

#import "UDGoogleDriveManager.h"

@interface UDGoogleDriveManager () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) GTLServiceDrive * client;
@property (nonatomic) BOOL updating;
@property (nonatomic) BOOL cycleUpdate;
@property (nonatomic) NSInteger connectionCount;
@property (nonatomic, strong) NSMutableArray * downloadList;
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation UDGoogleDriveManager

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static UDGoogleDriveManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _downloadList = [[NSMutableArray alloc] init];
        _client = [[GTLServiceDrive alloc] init];
        [_client setAuthorizer:[GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                                       clientID:GOOGLE_APP_KEY
                                                                                   clientSecret:GOOGLE_APP_SECRET]];
    }
    return self;
}

- (void)setCycleUpdate:(BOOL)cycleUpdate
{
    _cycleUpdate = cycleUpdate;
}

- (UIViewController*)loginCotroller
{
    return [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeDrive
                                                      clientID:GOOGLE_APP_KEY
                                                  clientSecret:GOOGLE_APP_SECRET
                                              keychainItemName:kKeychainItemName
                                                      delegate:self
                                              finishedSelector:@selector(viewController:finishedWithAuth:error:)];
}

- (void)updateFolder:(UDFolder*)parentFolder withID:(NSString*)driveID
{
    GTLQueryDrive * query = [GTLQueryDrive queryForFilesList];
    [query setQ:[NSString stringWithFormat:@"'%@' IN parents and trashed=false", driveID]];
    [_client executeQuery:query completionHandler:^(GTLServiceTicket * ticket, GTLDriveFileList * children, NSError * error) {
        if (error == nil)
        {
            for (GTLDriveFile * child in [children items])
            {
                NSString * childName = [child title];
                if ([[child mimeType] containsString:@"folder"])
                {
                    NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"name == %@", childName];
                    UDFolder *folder = [[[[parentFolder folders] filteredSetUsingPredicate:entityPredicate] allObjects] firstObject];
                    if (folder != nil)
                    {
                        [folder setForDeleteGoogleDrive:[NSNumber numberWithBool:NO]];
                        NSLog(@"Folder  %@  already exist", childName);
                    }
                    else
                    {
                        folder = [[UDCoreDataManager sharedInstance] newFolderWithName:childName];
                        [folder setPath:[NSString stringWithFormat:@"%@%@/", [parentFolder path], childName]];
                        [parentFolder addFoldersObject:folder];
                        NSLog(@"Folder  %@  created", childName);
                    }
                    [self updateFolder:folder withID:[child identifier]];
                    _connectionCount++;
                }
                else
                {
                    NSCompoundPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[NSPredicate predicateWithFormat:@"name == %@", childName],
                                                                                                           [NSPredicate predicateWithFormat:@"type == %d", GOOGLE_FILE]]];
                    UDFile * file = [[[[parentFolder files] filteredSetUsingPredicate:predicate] allObjects] firstObject];
                    if (file != nil)
                    {
                        [file setForDelete:[NSNumber numberWithBool:NO]];
                        if ([[file updatedDate] isEqualToDate:[[child modifiedDate] date]])
                        {
                            NSLog(@"File    %@  didn't change", childName);
                        }
                        else
                        {
                            [file setUpdatedDate:[[child modifiedDate] date]];
                            NSLog(@"File    %@  changed", childName);
                            [file setReady:[NSNumber numberWithBool:NO]];
                            [_downloadList addObject:file];
                            NSError *error;
                            if ([[NSFileManager defaultManager] removeItemAtPath:[file localPath] error:&error])
                            {
                                NSLog(@"File    %@  deleted from local path %@", file.name, file.localPath);
                            }
                            else
                            {
                                NSLog(@"Could not delete file from path %@ -:%@ ",[file localPath], [error localizedDescription]);
                            }
                        }
                    }
                    else
                    {
                        UDFile * file = [[UDCoreDataManager sharedInstance] newFileWithName:childName];
                        [file setType:[NSNumber numberWithInt:GOOGLE_FILE]];
                        [file setDrivePath:[child downloadUrl]];
                        [parentFolder addFilesObject:file];
                        [_downloadList addObject:file];
                        NSLog(@"File    %@  created", childName);
                    }
                }
            }
        }
        else
        {
            UDLog(@"Error in fetching %@", error);
        }
        _connectionCount--;
        if (_connectionCount == 0)
        {
            [[UDCoreDataManager sharedInstance] deleteAllSettedFiles:GOOGLE_FILE];
            [[UDCoreDataManager sharedInstance] deleteAllSettedFolders];
            [[UDCoreDataManager sharedInstance] saveContext];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
            [self loadFiles];
        }
    }];
}

- (void)loadFiles
{
    if ([_downloadList count] > 0)
    {
        UDFile * file = [_downloadList firstObject];
        GTMHTTPFetcher *fetcher = [_client.fetcherService fetcherWithURLString:[file drivePath]];
        
        [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
            if (error == nil)
            {
                NSString * localFolderPath = [NSString stringWithFormat:@"%@/GoogleDrive%@",[[UDCoreDataManager sharedInstance] applicationDocumentsDirectory], [[file parent] path]];
                NSString * filePath = [NSString stringWithFormat:@"%@%@", localFolderPath, [file name]];
                if (![[NSFileManager defaultManager] fileExistsAtPath:localFolderPath])
                {
                    NSError * error = nil;
                    if ([[NSFileManager defaultManager] createDirectoryAtPath:localFolderPath withIntermediateDirectories:NO attributes:nil error:&error])
                    {
                        UDLog(@"Folder created %@",localFolderPath);
                    }
                    else
                    {
                        UDLog(@"Could create a folder %@ %@",localFolderPath, error);
                    }
                }
                else
                {
                    UDLog(@"Folder exist %@",localFolderPath);
                }
                [file setLocalPath:filePath];
                [data writeToFile:[file localPath] atomically:YES];
                [file setReady:[NSNumber numberWithBool:YES]];
                UDLog(@"File %@ downloaded into %@", [file name], [file localPath]);
            }
            else
            {
                [file setUpdatedDate:[NSDate dateWithTimeIntervalSince1970:0]];
                [file setReady:[NSNumber numberWithBool:NO]];
                UDLog(@"Download file error occurred: %@", error);
            }
            [[UDCoreDataManager sharedInstance] saveContext];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
            [_downloadList removeObject:file];
            [self loadFiles];
        }];
    }
    else
    {
        UDLog(@"Google Drive Updated");
        _updating = NO;
        if (_cycleUpdate)
        {
            _timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                      target:self
                                                    selector:@selector(timerTick)
                                                    userInfo:nil
                                                     repeats:NO];
        }
    }
}

- (void)timerTick
{
    if (_cycleUpdate)
    {
        [self update];
    }
}

- (void)update
{
    if (!_updating)
    {
        [[UDCoreDataManager sharedInstance] setAllFilesToDelete:GOOGLE_FILE];
        [[UDCoreDataManager sharedInstance] setAllFoldersToDelete:GOOGLE_FILE];
        [_client setShouldFetchNextPages:YES];
        _updating = YES;
        [self updateFolder:[[UDCoreDataManager sharedInstance] getRootFolder] withID:GOOGLE_ROOT_FOLDER];
        _connectionCount++;
    }
}


#pragma mark -
#pragma mark - Google drive Delegate

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)authResult
                 error:(NSError *)error
{
    if (error == nil)
    {
        _client.authorizer = authResult;
        UDLog(@"Google drive success authorization");
    }
    else
    {
        _client.authorizer = nil;
        UDLog(@"Google drive authorization failed %@", error);
    }
}

@end
