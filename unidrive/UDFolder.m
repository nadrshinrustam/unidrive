//
//  UDFolder.m
//  
//
//  Created by Rustam Nadrshin on 5/21/15.
//
//

#import "UDFolder.h"
#import "UDFile.h"
#import "UDFolder.h"


@implementation UDFolder

@dynamic id;
@dynamic name;
@dynamic path;
@dynamic forDeleteDropbox;
@dynamic forDeleteOneDrive;
@dynamic forDeleteGoogleDrive;
@dynamic files;
@dynamic folders;
@dynamic parent;

@end
