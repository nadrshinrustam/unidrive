//
//  UDOneDriveManager.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/18/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#define ONE_DRIVE_APP_KEY       @"0000000048156E64"
#define ONE_DRIVE_APP_SECRET    @"EBiLbHNXjJFt9hxkBM0V7MuzjoCpD-WV"

#define ONE_DRIVE_ROOT_PATH     @"me/skydrive/"
#define ONE_DRIVE_ROOT_CONTENT  @"me/skydrive/files"

#define ONE_DRIVE_STATE_AUTH    @"auth"
#define ONE_DRIVE_STATE_LOGIN   @"login"

#define GET_FOLDER_CONTENT      @"folderContent"
#define DOWNLOAD_FILE           @"downloadFile"

#import "UDOneDriveManager.h"
#import <LiveSDK/LiveConnectClient.h>

@interface UDOneDriveManager () <LiveAuthDelegate, LiveOperationDelegate, LiveDownloadOperationDelegate>

@property (strong, nonatomic) LiveConnectClient * client;

@property (nonatomic) BOOL isLogged;
@property (nonatomic) BOOL updating;
@property (nonatomic) BOOL cycleUpdate;

@property (nonatomic) NSInteger connectionCount;

@property (nonatomic, strong) NSMutableArray * downloadList;
@property (nonatomic, strong) NSMutableDictionary * identifiers;
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation UDOneDriveManager

+ (id)sharedInstance
{
    static dispatch_once_t once;
    static UDOneDriveManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _client = [[LiveConnectClient alloc] initWithClientId:ONE_DRIVE_APP_KEY delegate:self userState:ONE_DRIVE_STATE_AUTH];
        _downloadList = [[NSMutableArray alloc] init];
        _identifiers = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void)setCycleUpdate:(BOOL)cycleUpdate
{
    _cycleUpdate = cycleUpdate;
}

- (void)loginFromCotroller:(UIViewController*)rootController
{
    NSArray * scopes = @[@"wl.signin", @"wl.basic", @"wl.skydrive", @"wl.offline_access"];
    [_client login:rootController scopes:scopes delegate:self userState:ONE_DRIVE_STATE_LOGIN];
}

- (void)update
{
    if (!_updating)
    {
        _updating = YES;
        [[UDCoreDataManager sharedInstance] setAllFilesToDelete:ONE_DRIVE_FILE];
        [[UDCoreDataManager sharedInstance] setAllFoldersToDelete:ONE_DRIVE_FILE];
        [_identifiers setObject:[[UDCoreDataManager sharedInstance] getRootFolder] forKey:ONE_DRIVE_ROOT_CONTENT];
        [_client getWithPath:ONE_DRIVE_ROOT_CONTENT delegate:self userState:GET_FOLDER_CONTENT];
        _connectionCount++;
    }
}

- (void)loadFiles
{
    UDFile * file = [_downloadList firstObject];
    if (file != nil)
    {
        NSString * parentPath = [[file parent] path];
        NSString * localPath = [NSString stringWithFormat:@"%@/OneDrive%@",[[UDCoreDataManager sharedInstance] applicationDocumentsDirectory], parentPath];
        NSString * filePath = [NSString stringWithFormat:@"%@%@", localPath, [file name]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:localPath])
        {
            NSError * error = nil;
            if ([[NSFileManager defaultManager] createDirectoryAtPath:localPath withIntermediateDirectories:NO attributes:nil error:&error])
            {
                UDLog(@"Folder created %@",localPath);
            }
            else
            {
                UDLog(@"Could create a folder %@ %@",localPath, error);
            }
        }
        else
        {
            UDLog(@"Folder exist %@",localPath);
        }
        UDLog(@"Local file path %@",filePath);
        [file setLocalPath:filePath];
        [_client downloadFromPath:[file drivePath] delegate:self userState:DOWNLOAD_FILE];
    }
    else
    {
        _updating = NO;
        UDLog(@"ONEDRIVE updated");
        if (_cycleUpdate)
        {
            _timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                      target:self
                                                    selector:@selector(timerTick)
                                                    userInfo:nil
                                                     repeats:NO];
        }
    }
}

- (void)timerTick
{
    if (_cycleUpdate)
    {
        [self update];
    }
}

- (BOOL)isFolder:(NSString*)type
{
    return [type isEqual:@"folder"] || [type isEqual:@"album"];
}

#pragma mark -
#pragma mark - Live Auth Delegate

- (void)authCompleted:(LiveConnectSessionStatus) status
              session:(LiveConnectSession *) session
            userState:(id) userState
{
    if ([userState isEqual:ONE_DRIVE_STATE_LOGIN])
    {
        UDLog(@"One drive authorized");
    }
    else if ([userState isEqual:ONE_DRIVE_STATE_LOGIN])
    {
        if (session != nil)
        {
            _isLogged = YES;
        }
    }
}

- (void)authFailed:(NSError *)error
         userState:(id)userState
{
    UDLog(@"%@ for state %@", error, userState);
}

#pragma mark -
#pragma mark - Live Operation Delegate

- (void)liveOperationSucceeded:(LiveOperation *)operation
{
    if ([[operation userState] isEqualToString:GET_FOLDER_CONTENT])
    {
        UDFolder * parentFolder = [_identifiers objectForKey:[operation path]];
        NSArray * content = [[operation result] objectForKey:@"data"];
        for (NSDictionary * object in content)
        {
            NSString * objectName = [object objectForKey:@"name"];
            if ([self isFolder:[object objectForKey:@"type"]])
            {
                NSPredicate *entityPredicate = [NSPredicate predicateWithFormat:@"name == %@", objectName];
                UDFolder * folder = [[[[parentFolder folders] filteredSetUsingPredicate:entityPredicate] allObjects] firstObject];
                if (folder != nil)
                {
                    [folder setForDeleteOneDrive:[NSNumber numberWithBool:NO]];
                    NSLog(@"Folder  %@  already exist", objectName);
                }
                else
                {
                    folder = [[UDCoreDataManager sharedInstance] newFolderWithName:objectName];
                    [folder setPath:[NSString stringWithFormat:@"%@%@/", [parentFolder path], [folder name]]];
                    [parentFolder addFoldersObject:folder];
                    NSLog(@"Folder  %@  created", objectName);
                }
                NSString * drivePath = [NSString stringWithFormat:@"%@/files", [object objectForKey:@"id"]];
                [_identifiers setObject:folder forKey:drivePath];
                [_client getWithPath:drivePath delegate:self userState:GET_FOLDER_CONTENT];
                _connectionCount++;
            }
            else
            {
                NSDateFormatter * dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
                NSDate * updatedDate = [dateFormat dateFromString:[object objectForKey:@"updated_time"]];
                NSCompoundPredicate * predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[[NSPredicate predicateWithFormat:@"name == %@", objectName],
                                                                                                       [NSPredicate predicateWithFormat:@"type == %d", ONE_DRIVE_FILE]]];
                UDFile * file = [[[[parentFolder files] filteredSetUsingPredicate:predicate] allObjects] firstObject];
                if (file != nil)
                {
                    [file setForDelete:[NSNumber numberWithBool:NO]];
                    if ([[file updatedDate] isEqualToDate:updatedDate])
                    {
                        NSLog(@"File    %@  didn't change", objectName);
                    }
                    else
                    {
                        [file setUpdatedDate:updatedDate];
                        NSLog(@"File    %@  changed", objectName);
                        [file setReady:[NSNumber numberWithBool:NO]];
                        [_downloadList addObject:file];
                        NSError *error;
                        if ([[NSFileManager defaultManager] removeItemAtPath:[file localPath] error:&error])
                        {
                            NSLog(@"File    %@  deleted from local path %@", file.name, file.localPath);
                        }
                        else
                        {
                            NSLog(@"Could not delete file from path %@ -:%@ ",[file localPath], [error localizedDescription]);
                        }
                    }
                }
                else
                {
                    UDFile * file = [[UDCoreDataManager sharedInstance] newFileWithName:objectName];
                    [file setType:[NSNumber numberWithInt:ONE_DRIVE_FILE]];
                    NSString * drivePath = [NSString stringWithFormat:@"%@/content", [object objectForKey:@"id"]];
                    [file setDrivePath:drivePath];
                    [file setUpdatedDate:updatedDate];
                    [parentFolder addFilesObject:file];
                    [[UDCoreDataManager sharedInstance] saveContext];
                    [_downloadList addObject:file];
                    NSLog(@"File    %@  created", objectName);
                }
            }
        }
        _connectionCount--;
        if (_connectionCount == 0)
        {
            [_identifiers removeAllObjects];
            [[UDCoreDataManager sharedInstance] deleteAllSettedFiles:ONE_DRIVE_FILE];
            [[UDCoreDataManager sharedInstance] saveContext];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
            [self loadFiles];
        }
    }
    else if ([[operation userState] isEqualToString:DOWNLOAD_FILE])
    {
        UDFile * file = [_downloadList firstObject];
        LiveDownloadOperation * downloadOperation = (LiveDownloadOperation *)operation;
        [[downloadOperation data] writeToFile:[file localPath] atomically:YES];
        [file setReady:[NSNumber numberWithBool:YES]];
        [[UDCoreDataManager sharedInstance] saveContext];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
        [_downloadList removeObject:file];
        [self loadFiles];
    }
}

- (void)liveOperationFailed:(NSError *)error operation:(LiveOperation *)operation
{
    if ([[operation userState] isEqualToString:GET_FOLDER_CONTENT])
    {
        UDLog(@"Error fetch %@", [operation path]);
        _connectionCount--;
        if (_connectionCount == 0)
        {
            [_identifiers removeAllObjects];
            [[UDCoreDataManager sharedInstance] deleteAllSettedFiles:ONE_DRIVE_FILE];
            [[UDCoreDataManager sharedInstance] saveContext];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
            [self loadFiles];
        }
    }
    else if ([[operation userState] isEqualToString:DOWNLOAD_FILE])
    {
        UDFile * file = [_downloadList firstObject];
        [file setUpdatedDate:[NSDate dateWithTimeIntervalSince1970:0]];
        [file setReady:[NSNumber numberWithBool:NO]];
        [[UDCoreDataManager sharedInstance] saveContext];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CONTEXT_UPDATED object:nil];
        [_downloadList removeObject:file];
        [self loadFiles];
    }
    UDLog(@"%@", error);
}

#pragma mark -
#pragma mark - Live Download Operation Delegate

- (void)liveDownloadOperationProgressed:(LiveOperationProgress *)progress
                                    data:(NSData *)receivedData
                               operation:(LiveDownloadOperation *)operation
{
    ;
}


@end
