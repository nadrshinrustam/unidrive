//
//  UDTableViewCell.m
//  unidrive
//
//  Created by Rustam Nadrshin on 5/16/15.
//  Copyright (c) 2015 Rustam Nadrshin. All rights reserved.
//

#import "UDTableViewCell.h"

@implementation UDTableViewCell

- (void)initWithName:(NSString*)name
{
    [_name setText:name];
    [_source setText:EMPTY_STRING];
}

- (void)initWithName:(NSString*)name andSource:(int)source;
{
    [_name setText:name];
    [_source setText:[self getFileSourceFromCode:source]];
}

- (NSString*)getFileSourceFromCode:(int)code
{
    NSString * source = nil;
    switch (code)
    {
        case GOOGLE_FILE:
            source =  @"G";
            break;
        case ONE_DRIVE_FILE:
            source = @"O";
            break;
        case DROPBOX_FILE:
            source =  @"D";
            break;
    }
    return source;
}

@end
