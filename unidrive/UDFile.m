//
//  UDFile.m
//  
//
//  Created by Rustam Nadrshin on 5/19/15.
//
//

#import "UDFile.h"
#import "UDFolder.h"


@implementation UDFile

@dynamic id;
@dynamic name;
@dynamic localPath;
@dynamic ready;
@dynamic type;
@dynamic drivePath;
@dynamic updatedDate;
@dynamic forDelete;
@dynamic parent;

@end
